/*
    ===== Código de TypeScript =====
*/

import { calculaISV, Producto } from "./06-desestructuracion-funcion";

const carritoCompra: Producto[] = [
    {
        desc: 'Telefono 1',
        precio: 99990
    },
    {
        desc: 'Telefono 2',
        precio: 149990
    },
];

const [total, isv] = calculaISV(carritoCompra);

console.log('Total: ' + total);
console.log('isv: ' + isv);