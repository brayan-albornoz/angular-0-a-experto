/*
    ===== Código de TypeScript =====
*/

interface Pasajero {
    nombre: string;
    hijos?: string[];   
}

const pasajero1: Pasajero = {
    nombre: 'Brayan'
}

const pasajero2: Pasajero = {
    nombre: 'Alexander',
    hijos: ['Teit', 'Wanda', 'Atreus']
}

function imprimeHijos(pasajero: Pasajero): void{
    const cuantosHijos = pasajero.hijos?.length || 0;
    console.log(cuantosHijos);
}

imprimeHijos(pasajero1);
imprimeHijos(pasajero2);