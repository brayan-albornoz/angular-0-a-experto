/*
    ===== Código de TypeScript =====
*/
function sumar(a: number, b: number) {
    return a + b;
}

const sumarFlecha = (a: number, b: number) => {
    return a + b;
}

function multiplicar(numero: number, otroNumero?: number, base: number = 2): number{
    return numero * base;
}

/* Pruebas para ejercicios - 03
const resultado = multiplicar(10, 0, 20);
console.log(resultado); */

interface PersonajeLOTR {
    nombre: string;
    pv: number;
    mostrarHp: () => void;
}

function curar(personaje: PersonajeLOTR, curarX): void{
    personaje.pv += curarX;
    console.log(personaje);
}

const nuevoPersonaje: PersonajeLOTR = {
    nombre: 'Strider',
    pv: 50,
    mostrarHp() {
        console.log('Puntos de vida: ' + this.pv);
    }
}

curar(nuevoPersonaje, 20);